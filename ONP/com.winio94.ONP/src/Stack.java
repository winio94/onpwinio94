package src;

/**Klasa tworzaca stos
 * @author Michal Winnicki
 */
public class Stack {

private int wskaznik_stosu = 0;
private int rozmiar;
private String[] stos;

/**
 * Ustalenie wielkosci stosu
 * @param rozmiar wielkosc stosu
 */
public Stack(int rozmiar) {
	this.rozmiar = rozmiar;
}

/**
 *  tworzenie stosu
 */
public void init_stack() {
	stos = new String[rozmiar];
}

/**dodawanie elementu do stosu
 * @param n dodawany element
 */
public void push(String n) { 
	if (!isfull())  //sprawdzam czy stos jest pelny
		stos[++wskaznik_stosu] = n;
	else
		System.out.println("Stos pelny!!!!!");
}
/**
 * pobieranie elementu ze stosu
 * @return zdejmowany element
 */
public String pop() {//sprawdzam czy stos jest pusty
	if (!isempty()) 
		return stos[wskaznik_stosu--];
	else {
		System.out.println("Stos pusty!!!!!!!!!");
		return null;
	}
}
/**
 * funkcja sprawdzajaca czy stos jest pusty
 * @return wartosc logiczna 
 */
public boolean isempty() { 
	if (wskaznik_stosu == 0)
		return true;
	else
		return false;
}
/**
 * funkcja sprawdzajaca czy stos jest pelny
 * @return wartosc logiczna 
 */
public boolean isfull() { //funkcja sprawdzajaca czy stos jest pelny
	if (wskaznik_stosu == rozmiar)
		return true;
	else
		return false;
}
/**
 * funkcja sprawdzajaca element lezacy na szczycie stosu
 * @return element ze szczytu stosu lub "stospusty", jesli stos jest pusty 
 */
public String peek_element() {
	if (!isempty()) 
		return stos[wskaznik_stosu];
	else 
		return "stospusty";
}
}

